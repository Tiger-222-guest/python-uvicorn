Source: python-uvicorn
Section: python
Priority: optional
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 debhelper (>= 11),
 dh-python,
 docbook-to-man,
 mkdocs (>= 1.0.4),
 python3-all,
 python3-click,
 python3-h11,
 python3-httptools,
 python3-pytest,
 python3-requests,
 python3-setuptools,
 python3-uvloop,
 python3-websockets,
 python3-wsproto,
Standards-Version: 4.2.1
Homepage: https://github.com/encode/uvicorn/
Vcs-Browser: https://salsa.debian.org/python-team/modules/python-uvicorn
Vcs-Git: https://salsa.debian.org/python-team/modules/python-uvicorn.git
Testsuite: autopkgtest-pkg-python

Package: python-uvicorn-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${mkdocs:Depends},
Description: ASGI server implementation, using uvloop and httptools (Documentation)
 Uvicorn is a fast ASGI server, built on uvloop and httptools. It currently
 supports HTTP/1.1 and WebSockets.
 .
 Uvicorn is designed with particular attention to connection and resource
 management, in order to provide a robust server implementation. It aims to
 ensure graceful behavior to either server or client errors, and resilience to
 poor client behavior or denial of service attacks.
 .
 This package contains the documentation.

Package: python3-uvicorn
Architecture: all
Depends:
 python3-wsproto,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-uvicorn-doc,
Description: ASGI server implementation, using uvloop and httptools (Python3 version)
 Uvicorn is a fast ASGI server, built on uvloop and httptools. It currently
 supports HTTP/1.1 and WebSockets.
 .
 Uvicorn is designed with particular attention to connection and resource
 management, in order to provide a robust server implementation. It aims to
 ensure graceful behavior to either server or client errors, and resilience to
 poor client behavior or denial of service attacks.
 .
 This package contains the Python 3 version of the library.

Package: uvicorn
Architecture: all
Section: httpd
Depends:
 python3-uvicorn (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-uvicorn-doc,
Description: ASGI server implementation, using uvloop and httptools
 Uvicorn is a fast ASGI server, built on uvloop and httptools. It currently
 supports HTTP/1.1 and WebSockets.
 .
 Uvicorn is designed with particular attention to connection and resource
 management, in order to provide a robust server implementation. It aims to
 ensure graceful behavior to either server or client errors, and resilience to
 poor client behavior or denial of service attacks.
 .
 This package contains the CLI script.
